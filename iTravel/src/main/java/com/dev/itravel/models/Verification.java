package com.dev.itravel.models;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Verification {

    private String code;

}
