package com.dev.itravel.controllers;

import com.dev.itravel.models.Offer;
import com.dev.itravel.models.User;
import com.dev.itravel.repos.OfferRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Controller
public class OfferController {

    private final OfferRepo offerRepo;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public OfferController(OfferRepo offerRepo) {
        this.offerRepo = offerRepo;
    }

    @GetMapping("/offers")
    public String main(@RequestParam(required = false, defaultValue = "") String filter, Model model) {
        if (filter != null && !filter.isEmpty()) model.addAttribute("offers", offerRepo.findByTag(filter));
        else model.addAttribute("offers", offerRepo.findAll());

        model.addAttribute("filter", filter);

        return "offers";
    }

    @PostMapping("add")
    public String addOffer(@AuthenticationPrincipal User user,
                           @RequestParam String text,
                           @RequestParam String tag, Map<String, Object> model,
                           @RequestParam("file") MultipartFile file) throws IOException {
        Offer offer = new Offer(text, tag, user);

        if (file != null && !Objects.requireNonNull(file.getOriginalFilename()).isEmpty()) {
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) uploadDir.mkdir();

            String uuid = UUID.randomUUID().toString();
            String filename = uuid + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + filename));
            offer.setFilename(filename);
        }

        offerRepo.save(offer);
        model.put("offers", offerRepo.findAll());

        return "offers";
    }

}
