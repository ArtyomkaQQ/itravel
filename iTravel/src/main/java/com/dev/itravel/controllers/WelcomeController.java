package com.dev.itravel.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

    @GetMapping
    public String greeting() {
        return "index";
    }

    @GetMapping("/login")
    public String loginForm() {
        return "login";
    }

}
