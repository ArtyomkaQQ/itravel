package com.dev.itravel.repos;

import com.dev.itravel.models.Offer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepo extends CrudRepository<Offer, Long> {

    List<Offer> findByTag(String tag);

}
