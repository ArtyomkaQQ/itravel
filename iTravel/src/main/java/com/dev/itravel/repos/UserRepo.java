package com.dev.itravel.repos;

import com.dev.itravel.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);
    User findByActivationCode(String code);

}
