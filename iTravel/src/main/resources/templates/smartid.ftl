<#import "macros/common.ftl" as c>

<@c.page>
    <div class="container">
        <div class="header clearfix">
            <h3 class="text-muted">Veebirakenduste loomine 2018</h3>
        </div>

        <div class="jumbotron">
            <div id="smart-id-login-container">
                <h1 class="display-3" id="main-text">Logi sisse</h1>
                <form method="post">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="countryCode" name="countryCode" placeholder="EE">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="nationalIdentityNumber" name="nationalIdentityNumber"
                                   aria-label="E-mail" placeholder="XXXXXXXXXXX">
                        </div>
                    </div>

                    <p><button type="submit" class="btn btn-lg btn-success">Start Smart-ID</button></p>
                </form>
            </div>
            <div id="smart-id-verification-code-container" style="display:none;">
                <h1 class="display-3" id="verification-code-text">XXXX</h1>
            </div>
        </div>

        <footer class="footer">
            <p>&copy; Tartu Ülikool 2018</p>
        </footer>

    </div>

    <script src="/webjars/jquery/jquery.min.js" defer ></script>

    <script src="../js/smartid.js" defer></script>
</@c.page>
