<#import "macros/common.ftl" as c>
<#import "macros/auth.ftl" as l>

<@c.page>
    <div class="mb-5"><h2> Add new user </h2></div>
    ${message?ifExists}
    <@l.login "/registration" true/>
</@c.page>
