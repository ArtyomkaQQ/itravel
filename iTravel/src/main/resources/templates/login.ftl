<#import "macros/common.ftl" as c>
<#import "macros/auth.ftl" as l>

<@c.page>
    ${message?ifExists}
    <@l.login "/login" false/>
    <a href="login/oauth2/code/177333407299-7tciosljpid69pchha33p76mctmtbbii.apps.googleusercontent.com"> Google Login </a>
    <a href="/smart-id/login"> Smart-ID login </a>
</@c.page>
