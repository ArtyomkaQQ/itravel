<#macro login path isRegisterForm>
    <div>
        <form action="${path}" method="post">
            <div class="form-group row"><label class="col-sm-2 col-form-label"> User Name: </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="username" placeholder="User Name"/>
                </div>
            </div>

            <#if isRegisterForm>
            <div class="form-group row"><label class="col-sm-2 col-form-label"> Email: </label>
                <div class="col-sm-5">
                    <input type="email" class="form-control" name="email" placeholder="some@some.com"/>
                </div>
            </div>
            </#if>

            <div class="form-group row"><label class="col-sm-2 col-form-label"> Password: </label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                </div>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <#if !isRegisterForm>
                <a href="/registration"> Add new user </a>
            </#if>
            <button type="submit" class="btn btn-primary"><#if isRegisterForm> Registrate <#else> Sign in </#if></button>
        </form>
    </div>
</#macro>

<#macro logout>
    <div>
        <form action="/logout" method="post">
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <button type="submit" class="btn btn-primary"> Sign Out </button>
        </form>
    </div>
</#macro>
