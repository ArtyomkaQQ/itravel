<#import "macros/common.ftl" as c>
<#import "macros/auth.ftl" as l>

<@c.page>
    <h5> ${username} </h5>
    ${message?ifExists}
    <div>
        <form method="post">
                <div class="form-group row"><label class="col-sm-2 col-form-label"> Email: </label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" name="email" placeholder="some@some.com" value="${email!''}"/>
                    </div>
                </div>

            <div class="form-group row"><label class="col-sm-2 col-form-label"> Password: </label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                </div>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button type="submit" class="btn btn-primary"> Save </button>
        </form>
    </div>
</@c.page>
