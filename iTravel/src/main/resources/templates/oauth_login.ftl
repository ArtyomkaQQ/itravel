<#import "macros/common.ftl" as c>

<@c.page>

    <h3>Login with:</h3>
    <#list urls as url_k, url_v>
        <#assign var_link = "${url_v}">
        <a href="${var_link}"> Client </a>
    </#list>
</@c.page>
