<#import "macros/common.ftl" as c>

<@c.page>
    <div class="form-row">
        <div class="form-group-col-md-5">
            <form method="get" action="/offers" class="form-inline">
                <input type="text" class="form-control" name="filter" value="${filter?ifExists}" placeholder="Search by tag">
                <button type="submit" class="btn btn-primary ml-2"> Search </button>
            </form>
        </div>
    </div>

    <a class="btn btn-primary mt-5" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
        Add new offer
    </a>
    <div class="collapse" id="collapseExample">
        <div class="form-group mt-3">
            <form method="post" action="/add" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="text" class="form-control" name="text" placeholder="Insert offer"/>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" name="tag" placeholder="Insert tag">
                </div>

                <div class="custom-file">
                    <input type="file" id="customFile" name="file">
                    <label class="custom-file-label" for="customFile"> Choose file </label>
                </div>

                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <div class="form-group mt-3">
                    <button type="submit" class="btn btn-primary"> Add </button>
                </div>
            </form>
        </div>
    </div>

    <div class="card-columns">
        <#list offers as offer>
            <div class="card my-3">
                <div class="card-img-top">
                    <#if offer.filename??>
                        <img src="/img/${offer.filename}">
                    </#if>
                </div>

                <div class="m-2">
                    <span>${offer.text}</span>
                    <i>${offer.tag}</i>
                </div>
                <div class="card-footer text-muted">
                    ${offer.authorName}
                </div>
            </div>
        <#else> No message
        </#list>
    </div>
</@c.page>
